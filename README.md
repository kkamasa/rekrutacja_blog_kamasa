# Instalacja #

composer install

php app/console doctrine:schema:update --force

php app/console doctrine:fixtures:load 

# Funkcjonalności #

### Ogólne ###
Projekt powinien wykorzystywać pakiet: friendsofsymfony/user-bundle (obsługa użytkowników,

logowanie oparte na bazie danych, wylogowanie). Proszę o dodanie do bazy danych dwóch aktywnych

użytkowników administracyjnych z loginem: „uzytkownik1” oraz „uzytkownik2” (hasła takie jak login; adres

e-mail dowolny).

Listy powinny być stronicowane po 5 elementów na stronie (z wykorzystaniem knplabs/knp-
paginator-bundle).

Dla poprawienia wyglądu aplikacji proszę wykorzystać bibliotekę Twitter Bootstrap 3, a interakcję po

stronie klienta zaimplementować z wykorzystaniem jQuery. Teksty pojawiające się na stronie powinny być

obsługiwane przez komponent tłumaczeń.
### Backend ###

Część administracyjna (URL zaczynające się od /admin) dostępna tylko i wyłącznie dla zalogowanych

użytkowników posiadających rolę ROLE_ADMIN, którzy mogą zarządzać treściami postów. Moduł do

zarządzania użytkownikami nie jest potrzebny. Zalogowany użytkownik może podejrzeć wszystkie posty, ale

edytować/usuwać tylko i wyłącznie własne.

Na moduł zarządzania postami składa się: lista wszystkich wpisów z tytułem, datą dodania, statusem (i datą

opóźnionej publikacji, jeżeli występuje) oraz linkiem do edycji. Nad listą proszę umieścić link do formularza

dodawania nowego wpisu.

Formularz dodawania/edycji wpisu do bazy danych (walidacja według wymagań) zawiera wszystkie

wymienione pola opócz daty dodania – ta ma być wstawiana automatycznie. Jeżeli dla postu zostanie

wybrany status „Opóźniona publikacjia” to powinna się pojawić sekcja z polem „data publikacji od”, dla

innych statusów sekcja jest niewidoczna a data pusta.
### Frontend ###

Lista wpisów zawierająca: tytuł (jako link do podglądu wpisu), datę dodania, autora oraz treść. Lista zawiera

tylko opublikowane wpisy posrotowane malejąco po dacie dodania. Podgląd wpisu z listą jego komentarzy

i formularzem dodania nowego komentarza (walidacja według wymagań). Dodawanie nowego komentarza

ma się odbywać bez przeładowania strony (z wykorzystaniem AJAX).

### CLI ###

Z poziomu wiersza poleceń jest dostępna komenda Symfony2: blog:post:publish-postponed, która

będzie zmieniała status postów ze statusem „Opóźniona publikacja” na „Opublikowany”, jeżeli data

i godzina uruchomienia jest późniejsza niż „data publikacji od” danego postu.