class CommentBox {
    constructor (selector, comments) {
        this.element = $(selector);
        this.comments = [];

        let self = this;
        comments.forEach(function (data) {
            self.newComment(data)
        });
    }

    newComment (data) {
        let comment = new Comment(data);
        this.comments.push(comment);
        this.appendComment(comment.render());

        return this;
    }

    appendComment (htmlComment) {
        this.element.prepend(htmlComment);

        return this;
    };
}

class Comment {
    constructor(data) {
        this.id = data.id;
        this.author = data.author;
        this.email = data.email;
        this.body = data.body;
        this.createdAt = moment(data.created_at);
    }

    render () {
        return $(`
            <li class="list-group-item">
                <div class="row">
                   <div class="col-md-2">
                       <div>${this.author}</div>
                       <div>${this.email}</div>
                    </div>
                   <div class="col-md-8">
                        <div>${this.body}</div>
                    </div> 
                    <div class="col-md-2">
                        <div>${this.createdAt.fromNow()}</div>
                    </div>
                </div>
            </li>
        `);
    }
}