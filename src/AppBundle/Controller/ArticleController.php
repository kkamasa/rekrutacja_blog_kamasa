<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\Comment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/article")
 *
 * Class ArticleController
 * @package AppBundle\Controller
 */
class ArticleController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:Article')
            ->getPublishedQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('article/index.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * @Route("/{id}")
     * @Method("GET")
     */
    public function showAction(Article $article)
    {
        $form = $this->createForm('AppBundle\Form\CommentType', null, [
            'action' => $this->generateUrl('app_article_addcomment', [
                'id' => $article->getId()
            ]),
        ]);

        return $this->render('article/show.html.twig', array(
            'commentForm' => $form->createView(),
            'article' => $article
        ));
    }

    /**
     * @Route("/{id}/add-comment")
     * @Method("POST")
     * @ParamConverter("article")
     *
     * @param Request $request
     * @param $article
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addCommentAction(Request $request, Article $article)
    {
        $em = $this->getDoctrine()->getManager();
        $serializer = $this->get('jms_serializer');
        $comment = new Comment();
        $comment->setArticle($article);

        $form = $this->createForm('AppBundle\Form\CommentType', $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($comment);
            $em->flush();

            return new Response(
                $serializer->serialize($comment, 'json')
            );
        }

        if (!$form->isValid()) {
            return new Response(
                $serializer->serialize($form, 'json')
                , Response::HTTP_BAD_REQUEST
            );
        }
    }
}
