<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

/**
 * Article controller.
 *
 * @Route("admin/article")
 */
class ArticleController extends Controller
{
    /**
     * Lists all article entities.
     *
     * @Route("/", name="admin_article_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $paginator  = $this->get('knp_paginator');
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:Article')
            ->createQueryBuilder('a')
            ->getQuery();

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('admin/article/index.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new article entity.
     *
     * @Route("/new", name="admin_article_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm('AppBundle\Form\ArticleType', $article);
        $form->handleRequest($request);
        $translator = $this->get('translator');

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush($article);

            $this->addFlash('success', $translator->trans('alert.success_create', [], 'base'));

            return $this->redirectToRoute('admin_article_index');
        }

        return $this->render('admin/article/form.html.twig', array(
            'article' => $article,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a article entity.
     *
     * @Route("/{id}", name="admin_article_show")
     * @Method("GET")
     */
    public function showAction(Article $article)
    {
        return $this->render('admin/article/show.html.twig', array(
            'article' => $article,
        ));
    }

    /**
     * Displays a form to edit an existing article entity.
     *
     * @Route("/{id}/edit", name="admin_article_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Article $article)
    {
        $this->denyAccessUnlessGranted('edit', $article);

        $form = $this->createForm('AppBundle\Form\ArticleType', $article);
        $form->handleRequest($request);
        $translator = $this->get('translator');

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $translator->trans('alert.success_save', [], 'base'));

            return $this->redirectToRoute('admin_article_index');
        }

        return $this->render('admin/article/form.html.twig', array(
            'form' => $form->createView(),
            'article' => $article
        ));
    }

    /**
     * Deletes a article entity.
     *
     * @Route("/{id}/delete", name="admin_article_delete")
     * @Method({"GET", "DELETE"})
     */
    public function deleteAction(Request $request, Article $article)
    {
        $this->denyAccessUnlessGranted('delete', $article);

        $translator = $this->get('translator');
        $form =$this->createFormBuilder()
            ->setMethod('DELETE')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush($article);
            $this->addFlash('success', $translator->trans('alert.success_delete', [], 'base'));

            return $this->redirectToRoute('admin_article_index');
        }

        return $this->render('admin/article/delete.html.twig', array(
            'form' => $form->createView(),
            'article' => $article
        ));
    }
}
