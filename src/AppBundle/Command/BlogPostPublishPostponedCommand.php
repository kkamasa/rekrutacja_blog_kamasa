<?php

namespace AppBundle\Command;

use AppBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BlogPostPublishPostponedCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('blog:post:publish-postponed');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $articles = $em->getRepository('AppBundle:Article')
            ->getPostponed();

        /** @var Article $article */
        foreach ($articles as $article) {
            $article->setPublishFrom(null);
            $article->setStatus(Article::STATUS_PUBLISHED);

            $em->persist($article);
        }

        $em->flush();

        $output->writeln('Updated '.count($articles).' articles');
    }
}
