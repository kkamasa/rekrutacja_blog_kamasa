<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setUsername('uzytkownik1');
        $user1->setEmail('uzytkownik1@exapmple.com');
        $user1->setPlainPassword('uzytkownik1');
        $user1->setEnabled(true);
        $user1->setAdmin(true);

        $user2 = new User();
        $user2->setUsername('uzytkownik2');
        $user2->setEmail('uzytkownik2@exapmple.com');
        $user2->setPlainPassword('uzytkownik2');
        $user2->setEnabled(true);
        $user2->setAdmin(true);

        $manager->persist($user1);
        $manager->persist($user2);
        $manager->flush();
    }
}