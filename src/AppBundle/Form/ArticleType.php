<?php

namespace AppBundle\Form;

use AppBundle\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'form.label_title',
                'translation_domain' => 'admin_article',
            ])
            ->add('body', null, [
                'label' => 'form.label_body',
                'translation_domain' => 'admin_article',
            ])
            ->add('status', ChoiceType::class, [
                'label' => 'form.label_status',
                'translation_domain' => 'admin_article',
                'choice_translation_domain' => 'admin_article',
                'choices' => Article::$statusNames,
            ])
            ->add('publishFrom', DateType::class, [
                'label' => 'form.label_publish_from',
                'translation_domain' => 'admin_article',
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
                'required' => false,
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'dd.mm.yyyy'
                ]
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();

            if ($data['status'] != Article::STATUS_WAITING) {
                unset($data['publishFrom']);
                $event->setData($data);
            } else {
                $options = $form->get('publishFrom')->getConfig()->getOptions();
                $form->remove('publishFrom');
                $options['constraints'] =  new NotBlank();
                $form->add('publishFrom', DateType::class, $options);
            }
        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Article'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_article';
    }


}
